
export const updateCount = (dispatch, value) => {
  dispatch({
    type: 'UPDATE_COUNT_ADD',
    count: value
  })
}