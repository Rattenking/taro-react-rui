import { createContext, useReducer, useContext } from "react";

let state = {
  count: 0,
  ischeck: false
}

export let CountContext = createContext({});

export let UPDATE_COUNT = 'UPDATE_COUNT';
export let UPDATE_COUNT_ADD = 'UPDATE_COUNT_ADD';
export let UPDATE_COUNT_PREV = 'UPDATE_COUNT_PREV';

let reducer = (state, action) => {
  switch(action.type){
    case UPDATE_COUNT_ADD:
      console.log('UPDATE_COUNT_ADD')
      return action.count
    case UPDATE_COUNT_PREV:
      console.log('UPDATE_COUNT_PREV')
      return action.count
    default:
        return state
  }
}

export const Count = props=>{
  let [count, dispatch] = useReducer(reducer, 0);
  return (
    <CountContext.Provider value={{count,dispatch}}>
      {props.children}
    </CountContext.Provider>
  )
}

