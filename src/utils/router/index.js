/*
 * @Description: 跳转方法封装
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-10-18 10:25:31
 * @LastEditors: Rattenking
 * @LastEditTime: 2022-10-18 10:32:33
 */
import Taro from "@tarojs/taro";

const types = {
  navigateTo: "navigateTo",
  switchTab: "switchTab",
  reLaunch: "reLaunch",
  redirectTo: "redirectTo",
  navigateBack: "navigateBack",
};

const routeTo = (opts) => {
  let {
    type = "navigateTo",
    url = "",
    appid = "",
    delta,
    isVerifyLogin = false,
  } = opts;
  $route({ ...opts, type, url, appid, delta, isVerifyLogin });
};

/**
 * 1. 判断传入url是否有效
 * 2. 判断是小程序路径还是外部H5
 * 3. 判断是返回还是直接为空字符串
 * */
const $route = (opts) => {
  let {
    type = "navigateTo",
    url = "",
    appid = "",
    delta,
    isVerifyLogin = false,
  } = opts;
  if (url) {
    if (url.indexOf("http") > -1) {
      if (api.hasWeapp()) {
        Taro.setStorageSync("urlAndParams", `${url}`);
        if (url.indexOf("islogin=1") > -1) {
          route({ url: "../../pages/payLogin/index", type });
        } else {
          route({ url: "../../pages/pay/index", type });
        }
      } else {
        window.location.href = url;
      }
    } else {
      route(opts);
    }
  } else {
    delta ? Taro.navigateBack(opts) : "";
  }
};

const route = (opts) => {
  let { type = "navigateTo", url = "" } = opts;
  type = types[type] ? type : "navigateTo";
  Taro[type].call(Taro, { url });
};

export default {
  routeTo,
  $route,
};