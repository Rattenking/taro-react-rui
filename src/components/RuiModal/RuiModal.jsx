/*
 * @Description: 模态框
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-10-19 17:03:15
 * @LastEditors: Rattenking
 * @LastEditTime: 2022-10-20 10:02:49
 */
import React, { Component } from "react";
import { View, Text } from "@tarojs/components";
import RuiOverlay from "../RuiOverlay/RuiOverlay";
import './index.scss';

class RuiModal extends Component {
  constructor(props) {
    super(props);
    let { isOpened } = this.props;
    this.state = {
      _isOpened: !!isOpened
    }
  }
  UNSAFE_componentWillReceiveProps(nextProps){
    const { isOpened } = nextProps;
    const { _isOpened } = this.state;
    if (isOpened !== _isOpened) { 
      this.setState({_isOpened: !!isOpened })
    }
  }
  // 自动关闭判断
  hasAutoClose () {
    let { autoClose = false } = this.props;
    console.log(autoClose)
    if (autoClose) {
      this.setState({ _isOpened: false });
    }
  }
  // 关闭提示框
  closeModal () {
    let {onClose = () => { }} = this.props;
    onClose.call(this);
    this.hasAutoClose();
  }
  // 取消按钮事件
  cancelModal () {
    let {onCancel = () => { }} = this.props;
    onCancel.call(this);
    this.hasAutoClose();
  }
  // 确认按钮事件
  confirmModal () {
    let {onConfirm = () => { }} = this.props;
    onConfirm.call(this);
    this.hasAutoClose();
  }
  render () { 
    let {
      title = '',
      content = '',
      cancelText = '',
      confirmText = '',
      isShowCloseBtn = true,
      closeOnClickOverlay = false,
      children
    } = this.props;
    let { _isOpened } = this.state;
    return <RuiOverlay
      isOpened={_isOpened}
      onClose={closeOnClickOverlay ? this.closeModal.bind(this) : null}>
      <View className="rui-modal-component-content">
        <View className="rui-modal-mask-content">
          {/* 标题位置 */}
          {title ? <View className="rui-fs32 rui-tac">{title}</View> : <React.Fragment />}
          
          {/* 关闭按钮 */}
          {isShowCloseBtn ? <Text className='rui-icon rui-icon-close rui-modal-mask-icon rui-fs30'
            onClick={this.closeModal.bind(this)}></Text> : <React.Fragment />}
          
          {/* 内容位置 */}
          {content ? <View className="rui-fs30 rui-tac">{content}</View> : <React.Fragment />}
          
          {/* 传入的代码显示 */}
          <React.Fragment>
            {children}
          </React.Fragment>

          {/* 取消和确认按钮 */}
          {cancelText || confirmText ?
            <View className="rui-flex-sb rui-mt80">
              {/* 取消按钮 */}
              {cancelText ? <View className="rui-modal-mask-btn"
                onClick={this.cancelModal.bind(this)}>{cancelText}</View> : <React.Fragment />}
              
              {/* 确认按钮 */}
              {confirmText ? <View className="rui-modal-mask-btn rui-bg11"
                onClick={this.confirmModal.bind(this)}>{confirmText}</View> : <React.Fragment />}
              </View> : <React.Fragment />}
        </View>
      </View>
    </RuiOverlay>;
  }
}

export default RuiModal;