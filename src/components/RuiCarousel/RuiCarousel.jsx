import { View } from "@tarojs/components";
import "./index.scss";

const RuiCarousel = ({ children, speed = "20s" }) => {
  return (
    <View className="rui-carousel-container">
      <View className="rui-carousel-track" style={{ "--time": speed }}>
        {children}
        {children}
      </View>
    </View>
  );
};

export default RuiCarousel;
