/*
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-11-09 10:06:42
 */
import React, { Component } from "react";
import { View, Text, ScrollView } from "@tarojs/components";
import './index.scss';

class RuiIndexes extends Component {
  constructor(props) {
    super(props);
  }
  state = {  }
  render () {
    let { children } = this.props;
    return <ScrollView scrollY className="rui-indexes-component-content">
      {children}
    </ScrollView>;
  }
}

export default RuiIndexes;