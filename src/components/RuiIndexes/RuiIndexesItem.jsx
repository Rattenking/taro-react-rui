/*
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-11-09 10:47:39
 */
import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View, Text, ScrollView } from "@tarojs/components";
import './index.scss';

class RuiIndexesItem extends Component {
  constructor(props) {
    super(props);
  }
  state = {  }
  render () { 
    let { children, id="" } = this.props;
    return <View id={`rui-default-${id}`}>
      { children }
    </View>;
  }
}

export default RuiIndexesItem;