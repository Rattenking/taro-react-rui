/*
 * @Description: Taro React Rui!
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-10-18 09:40:46
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2023-01-12 10:09:33
 */
export default {
  pages: [
    "pages/index/index",
    "pages/indexes/index",
    "pages/indexes/base",
    "pages/overlay/index",
    "pages/modal/index",
    "pages/swipeAction/index",
    "pages/game/index",
    "pages/game/removeFiveBead",
    "pages/game/mineSweeper",
    "pages/game/snakeGame",
    "pages/game/tetrisGame",
    "pages/game/gobang",
    "pages/clipIcons/index",
    "pages/context/classContext",
    "pages/context/hookContext",
    "pages/signature/index",
    "pages/signature/limit",
    "pages/signature/btn",
    "pages/signature/loading",
    "pages/loading/index",
    "pages/willChange/index"
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "Taro React Rui!",
    navigationBarTextStyle: "black"
  }
}
