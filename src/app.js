import { Component } from 'react'
import './app.scss'
import router from './utils/router/index';
import {
  useQuery,
  useMutation,
  useQueryClient,
  QueryClient,
  QueryClientProvider,
} from 'react-query'
const queryClient = new QueryClient()
// 全局跳转
Component.prototype.routeTo = router.routeTo;
Component.prototype.$route = router.$route;

class App extends Component {

  // this.props.children 是将要会渲染的页面
  render () {
    return <QueryClientProvider client={queryClient}>{ this.props.children }</QueryClientProvider>
  }
}

export default App
