import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View } from "@tarojs/components";
import { ClassCreateContext } from './classCreateContext';

class ClassConsumerContext extends Component {
  constructor(props) {
    super(props);
  }
  state = {  }
  render() { 
    return <ClassCreateContext.Consumer>
      {
        value => (
          <>
            <View>类组件【ClassConsumerContext】</View>
            <View>获取上下文 count: {value.count}</View>
            <View>修改父组件的 count</View>
            <View className='rui-flex-ac'>
              <View 
                onClick={value.updateCount.bind(null, 'add')}
                className='rui-flex-cc rui-fg'>consumer 加</View>
              <View 
                onClick={value.updateCount.bind(null, 'minus')}
                className='rui-flex-cc rui-fg'>consumer 减</View>
            </View>
          </>
        )
      }
    </ClassCreateContext.Consumer>;
  }
}

export default ClassConsumerContext;