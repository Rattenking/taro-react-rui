import { View, Text, Image } from '@tarojs/components';
import { useContext } from 'react';
import { HookCreateContext } from './hookCreateContext';

const HookSonContext = (props) => {
  let { count, updateCount } = useContext(HookCreateContext)
  return <View>
    <View>函数组件【HookSonContext】</View>
    <View>获取上下文 count: {count}</View>
    <View>修改父组件的 count</View>
    <View className='rui-flex-ac'>
      <View 
        onClick={updateCount.bind(null, 'add')}
        className='rui-flex-cc rui-fg'>hook 加</View>
      <View 
        onClick={updateCount.bind(null, 'minus')}
        className='rui-flex-cc rui-fg'>hook 减</View>
    </View>
  </View>
}
export default HookSonContext;