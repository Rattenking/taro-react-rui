import { View, Text, Image } from '@tarojs/components';
import { useContext } from 'react';
import { ClassCreateContext } from './classCreateContext';

const HookChildContext = (props) => {
  let { count, updateCount } = useContext(ClassCreateContext)
  return <View>
    <View>函数组件【HookChildContext】</View>
    <View>获取上下文 count: {count}</View>
    <View>修改父组件的 count</View>
    <View className='rui-flex-ac'>
      <View 
        onClick={updateCount.bind(null, 'add')}
        className='rui-flex-cc rui-fg'>hook 加</View>
      <View 
        onClick={updateCount.bind(null, 'minus')}
        className='rui-flex-cc rui-fg'>hook 减</View>
    </View>
  </View>
}
export default HookChildContext;