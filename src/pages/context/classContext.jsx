import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View } from "@tarojs/components";
import { ClassCreateContext } from './classCreateContext';
import HookChildContext from './hookChildContext';
import ClassChildContext from './classChildContext';
import ClassConsumerContext from './classConsumerContext';

class ClassContext extends Component {
  constructor(props) {
    super(props);
  }
  state = {  
    count: 0
  }
  // 修改count的值
  updateCount = (type = 'add') => {
    let { count } = this.state;
    count = {
      'add': () => {
        return ++count
      },
      'minus': () => {
        return --count
      }
    }[type]?.()
    this.setState({count})
  }
  render() { 
    let { count } = this.state;
    return <View>
      {/* 类组件的父组件的变量显示和操作 */}
      <View>父组件 count: {count}</View>
      <View className='rui-flex-ac'>
        <View 
          className='rui-fg rui-flex-cc' 
          onClick={this.updateCount.bind(this,'add')}>加</View>
        <View 
          className='rui-fg rui-flex-cc'
          onClick={this.updateCount.bind(this,'minus')}>减</View>
      </View>
      {/* 创建上下文并提供值 */}
      <ClassCreateContext.Provider value={{ count, updateCount: this.updateCount }}>
        {/* 类组件中使用函数组件访问上下文 */}
        <HookChildContext/>
        {/* 类组件中使用类组件使用 static contextType 访问上下文 */}
        <ClassChildContext/>
        {/* 类组件中使用类组件使用 <XxxContext.Consumer> 访问上下文 */}
        <ClassConsumerContext/>
      </ClassCreateContext.Provider>
    </View>;
  }
}

export default ClassContext;