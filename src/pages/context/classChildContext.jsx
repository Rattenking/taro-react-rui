import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View } from "@tarojs/components";
import { ClassCreateContext } from './classCreateContext';

class ClassChildContext extends Component {
  static contextType = ClassCreateContext;
  constructor(props) {
    super(props);
  }
  state = {  }
  render() { 
    let { count, updateCount } = this.context;
    return <View>
      <View>类组件【ClassChildContext】</View>
      <View>获取上下文 count: {count}</View>
      <View>修改父组件的 count</View>
      <View className='rui-flex-ac'>
        <View 
          onClick={updateCount.bind(null, 'add')}
          className='rui-flex-cc rui-fg'>class 加</View>
        <View 
          onClick={updateCount.bind(null, 'minus')}
          className='rui-flex-cc rui-fg'>class 减</View>
      </View>
    </View>;
  }
}

export default ClassChildContext;