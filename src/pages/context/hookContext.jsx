import { View, Text, Image } from '@tarojs/components';
import { useState, useEffect } from 'react';
import { HookCreateContext } from './hookCreateContext';
import ClassSonContext from './classSonContext';
import HookSonContext from './HookSonContext';

const HookContext = (props) => {
  let [count, setCount] = useState(0)
  // 修改 count
  function updateCount(type='add'){
    count = {
      'add': () => {
        console.log(count)
        return ++count
      },
      'minus': () => {
        return --count
      }
    }[type]?.()
    console.log('count',count)
    setCount(count)
  }
  return <View>
    <View>父组件 count: {count}</View>
    <View className='rui-flex-ac'>
      <View 
        onClick={updateCount.bind(null, 'add')}
        className='rui-flex-cc rui-fg'>加</View>
      <View 
        onClick={updateCount.bind(null, 'minus')}
        className='rui-flex-cc rui-fg'>减</View>
    </View>
    <HookCreateContext.Provider value={{count, updateCount}}>
      <ClassSonContext/>
      <HookSonContext/>
    </HookCreateContext.Provider>
  </View>
}
export default HookContext;