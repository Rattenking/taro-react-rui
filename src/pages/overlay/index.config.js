/*
 * @Description: 遮罩层
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-10-18 09:55:14
 * @LastEditors: Rattenking
 * @LastEditTime: 2022-10-18 09:55:36
 */
export default {
  navigationBarTitleText: "遮罩层",
  navigationStyle: "custom",
};