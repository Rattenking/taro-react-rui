/*
 * @Description: 遮罩层
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-10-18 09:52:25
 * @LastEditors: Rattenking
 * @LastEditTime: 2022-10-20 10:03:44
 */
import React, { Component } from "react";
import { View, Text } from '@tarojs/components';
import RuiOverlay from "../../components/RuiOverlay/RuiOverlay";
import './index.scss';


class Overlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowBase: false,
      isShowContent: false,
      isShowBottom: false,
      isShowOpacity: false,
      isShowDuration: false,
    }
  }
  render () {
    let {
      isShowBase,
      isShowContent,
      isShowBottom,
      isShowOpacity,
      isShowDuration
    } = this.state;
    return <View className="rui-overlay-page-content">
      {/* 基本案例 */}
      <RuiOverlay isOpened={isShowBase}
        onClose={() => { this.setState({ isShowBase: false }) }} />
      
      {/* 嵌入内容居中 */}
      <RuiOverlay isOpened={isShowContent}
        onClose={() => { this.setState({isShowContent: false}) }}>
        <View className="rui-flex-cc"
          style={`background:#fff;width:100px;height:100px;`}>123456</View>
      </RuiOverlay>

      {/* 嵌入内容底部 */}
      <RuiOverlay isOpened={isShowBottom}
        position="bottom"
        onClose={() => { this.setState({isShowBottom: false}) }}>
        <View className="rui-flex-cc"
          style={`background:#fff;width:100%;height:50vh;`}>123456</View>
      </RuiOverlay>

      {/* 设置透明度 */}
      <RuiOverlay isOpened={isShowOpacity}
        onClose={() => { this.setState({isShowOpacity: false}) }}
        opacity="0.5" />

      {/* 设置动画时间 */}
      <RuiOverlay isOpened={isShowDuration}
        duration={1000}
        onClose={() => { this.setState({isShowDuration: false}) }}/>
      
      <View className="rui-flex-ac rui-list-item"
        onClick={() => { this.setState({isShowBase: true}) }}>
        <Text className="rui-fs30 rui-fg">基本案例</Text>
        <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
      </View>
      <View className="rui-flex-ac rui-list-item"
        onClick={() => { this.setState({isShowContent: true}) }}>
        <Text className="rui-fs30 rui-fg">嵌入内容居中</Text>
        <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
      </View>
      <View className="rui-flex-ac rui-list-item"
        onClick={() => { this.setState({isShowBottom: true}) }}>
        <Text className="rui-fs30 rui-fg">嵌入内容底部</Text>
        <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
      </View>
      <View className="rui-flex-ac rui-list-item"
        onClick={() => { this.setState({isShowOpacity: true}) }}>
        <Text className="rui-fs30 rui-fg">设置透明度</Text>
        <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
      </View>
      <View className="rui-flex-ac rui-list-item"
        onClick={() => { this.setState({isShowDuration: true}) }}>
        <Text className="rui-fs30 rui-fg">设置动画时间</Text>
        <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
      </View>
    </View>
  }
}

export default Overlay;