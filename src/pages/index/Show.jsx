import { View, Text, Image } from '@tarojs/components';
import { useDidShow, useRouter } from '@tarojs/taro';
import { useState, useEffect, useContext } from 'react';
import './index.scss';
import { CountContext } from '@src/store/countReducer';

const Show = (props) => {
  const { count } = useContext(CountContext)
  console.log(count)
  return <View className='rui-fs60'>{ count }</View>
}
export default Show;