/*
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-11-09 10:32:39
 */
const menuList = [
  {
    title: 'Modal 模态框',
    url: '../../pages/modal/index'
  },
  {
    title: 'Overlay 遮罩层',
    url: '../../pages/overlay/index'
  },
  {
    title: 'Indexes 索引选择器',
    url: '../../pages/indexes/index'
  },
  {
    title: '类组件访问上下文',
    url: '../../pages/context/classContext'
  },
  {
    title: '函数组件访问上下文',
    url: '../../pages/context/hookContext'
  },
  {
    title: 'Clip-Path 图标',
    url: '../../pages/clipIcons/index'
  },
  {
    title: '签名',
    url: '../../pages/signature/index'
  },
  {
    title: '文本显示行数',
    url: '../../pages/signature/limit'
  },
  {
    title: '按钮',
    url: '../../pages/signature/btn'
  },
  {
    title: '加载',
    url: '/pages/loading/index'
  },
  {
    title: '无限滚动轮播的 CSS 实现',
    url: '/pages/willChange/index'
  },
]
export default menuList;