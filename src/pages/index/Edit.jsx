import { View, Text, Image } from '@tarojs/components';
import { useState, useEffect, useContext } from 'react';
import { CountContext } from '@src/store/countReducer';
import { updateCount } from '@src/store/update';
import './index.scss';
// import { createAlova, useRequest } from 'alova';
import { useQuery } from "react-query";
import axios from '@utils/axios'
axios.defaults.baseURL = 'https://api.github.com'
//请求拦截器
axios.interceptors.request.use(config=>{
    console.log('请求配置信息：',config);
    return config
})

axios.interceptors.request.use(config=>{
    // config.headers.token12 = '654321';
  config.token123 = '456';
    return config
})

//响应拦截器
axios.interceptors.response.use(res=>{
    console.log('请求响应信息',res)
    return res;
})

axios.interceptors.response.use(res=>{
    res.msg = 'request is ok ~';
    return res;
})

const Edit = (props) => {
  const { dispatch, count } = useContext(CountContext)
  // console.log('Edit',count)
  const { data, isLoading, error } = useQuery("getGithubStar", () =>
    // axios.get("https://api.github.com/repos/tannerlinsley/react-query")
    fetch("https://api.github.com/repos/tannerlinsley/react-query").then(fetchRes => {
      return fetchRes.json()
    })
  );
  axios.get('/repos/tannerlinsley/react-query').then(res => {
    console.log(res)
  })
  console.log(data)
  console.log(isLoading)
  console.log(error)
  return <View> {isLoading ? '加载中...' : `加载完成${data.stargazers_count}`}
    <View className='rui-fs30' onClick={() => {
      updateCount(dispatch, count++)
    }}>加</View>
    <View className='rui-fs30' onClick={() => {
      updateCount(dispatch, count--)
    }}>减</View>
  </View>
}
export default Edit;