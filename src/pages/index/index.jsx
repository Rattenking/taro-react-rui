/*
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-10-18 09:40:46
 */
import { Component } from "react";
import { View, Text } from "@tarojs/components";
import menuList from "./menuList";
import "./index.scss";

export default class Index extends Component {
  render() {
    return (
      <View className="rui-index-page-content">
        <View className="rui-list-content">
          {menuList.map((item, index) => (
            <View
              className="rui-flex-ac rui-list-item"
              key={item + index}
              onClick={this.$route.bind(this, { url: item.url })}
            >
              <Text className="rui-fs30 rui-fg rui-line1">{item.title}</Text>
              <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
            </View>
          ))}
        </View>
      </View>
    );
  }
}
