/*
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2023-01-12 10:07:04
 */
import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import './index.scss';

class ClipIcons extends Component {
  constructor(props) {
    super(props);
  }
  state = {}
  value () {
    <svg t="1673490867097" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="872" width="200" height="200"><path d="M928.064 512a32 32 0 0 0-32 32V896h-768V128h352a32 32 0 0 0 0-64h-352a64 64 0 0 0-64 64v768a64 64 0 0 0 64 64h768a64 64 0 0 0 64-64V544a32 32 0 0 0-32-32z" p-id="873"></path><path d="M457.408 566.592a32 32 0 0 0 45.248 0l448-448a32 32 0 0 0-45.248-45.184l-448 448a32 32 0 0 0 0 45.184z" p-id="874"></path></svg>
  }
  render() { 
    return <View className="rui-clip-path-page-content">
      <View className="rui-fs40">导航图标</View>
      <View className="rui-flex-fw">
        <View className="">
          <Text className="rui-icon icon-arrow-up rui-fs60"></Text>
        </View>
      </View>
    </View>;
  }
}

export default ClipIcons;