/*
 * @Description: 遮罩层
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-10-18 09:55:14
 * @LastEditors: Rattenking 1148063373@qq.com
 * @LastEditTime: 2022-10-28 15:34:37
 */
export default {
  navigationBarTitleText: "滑动单元格",
  navigationStyle: "custom",
};