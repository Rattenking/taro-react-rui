import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View } from "@tarojs/components";
import './index.scss';

class SwipeAction extends Component {
  constructor(props) {
    super(props);
  }
  state = {  }
  render() { 
    return <View className="rui-swipe-action-page-content">
      <View className="rui-fs40">1. 纯滚动</View>
      <View className="list">
        <View className="button rui-danger rui-flex-cc">删除</View>
        <View className="content">我是列表，试试左滑我</View>
        <View className="space"></View>
      </View>
      <View className="rui-fs40">2. 带定位滚动</View>
      <View className="list">
        <View className="button rui-danger rui-flex-cc">删除</View>
        <View className="content">我是列表，试试左滑我</View>
        <View className="space"></View>
      </View>
      <View className="rui-fs40">3. 多个按钮定位滚动</View>
      <View className="list scroll-snap perspective">
        <View className="button rui-danger rui-flex-cc">删除</View>
        <View className="content">我是列表，试试左滑我</View>
        <View className="space"></View>
      </View>
      <View className="rui-fs40">4. 多个按钮定位视差滚动</View>
      <View className="list scroll-snap perspective">
        <View className="rui-button-content rui-flex-content">
          <View className="rui-button rui-danger rui-flex-cc">删除</View>
          <View className="rui-button rui-warning rui-flex-cc">不显示</View>
        </View>
        <View className="content">
          <View className="rui-fs30">我是列表，试试左滑我</View>
        </View>
        <View className="rui-space"></View>
      </View>
    </View>;
  }
}

export default SwipeAction;