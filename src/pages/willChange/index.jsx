import { View, Image } from "@tarojs/components";
import "./index.scss";

export default function WillChange() {
  return (
    <View className="rui-will-change-page-content rui-fs26">
      <View className="rui-fwb rui-fs30 rui-mb30">无限滚动轮播 演示</View>
      <View
        className="rui-will-change-content"
        style={{
          overflow: "visible",
          border: "4px solid blue",
          padding: "20px 0",
        }}
      >
        <View
          className="rui-grid-content"
          style={{ border: "4px solid red", width: "150vw" }}
        >
          第一组元素
        </View>
        <View
          className="rui-grid-content"
          style={{ border: "4px solid green", width: "150vw" }}
        >
          第二组元素（复制元素）
        </View>
      </View>
      <View className="rui-fwb rui-fs30 rui-mb30">无限滚动轮播的 CSS 实现</View>
      <View className="rui-will-change-content">
        <View className="rui-grid-content">
          <Image
            src={`https://img14.360buyimg.com/mobilecms/s360x360_jfs/t1/252666/12/28906/128995/67c70923F7ea34dfc/8ac042791e1dbae8.jpg!q70.dpg.webp`}
            className="rui-grid-item"
          ></Image>
          <Image
            src={`https://img14.360buyimg.com/mobilecms/s360x360_jfs/t1/262472/10/27398/106649/67c551a9F4eb82724/5b9f7cd6b67b1101.jpg!q70.dpg.webp`}
            className="rui-grid-item"
          ></Image>
          <Image
            src={`https://img14.360buyimg.com/mobilecms/s360x360_jfs/t1/266314/35/9312/155449/677cf57eF804de0b9/e709efdffed05aef.jpg!q70.dpg.webp`}
            className="rui-grid-item"
          ></Image>
        </View>
        <View className="rui-grid-content">
          <Image
            src={`https://img14.360buyimg.com/mobilecms/s360x360_jfs/t1/252666/12/28906/128995/67c70923F7ea34dfc/8ac042791e1dbae8.jpg!q70.dpg.webp`}
            className="rui-grid-item"
          ></Image>
          <Image
            src={`https://img14.360buyimg.com/mobilecms/s360x360_jfs/t1/262472/10/27398/106649/67c551a9F4eb82724/5b9f7cd6b67b1101.jpg!q70.dpg.webp`}
            className="rui-grid-item"
          ></Image>
          <Image
            src={`https://img14.360buyimg.com/mobilecms/s360x360_jfs/t1/266314/35/9312/155449/677cf57eF804de0b9/e709efdffed05aef.jpg!q70.dpg.webp`}
            className="rui-grid-item"
          ></Image>
        </View>
      </View>

      <View className="rui-fwb rui-fs30 rui-mb30 rui-mt30">公告栏实现</View>
      <View className="rui-will-change-content">
        <View className="rui-flex-content">
          <View className="rui-flex-item">
            字节跳动于3月3日发布国内首款 AI 原生集成开发环境工具1。
          </View>
          <View className="rui-flex-item">
            字节跳动于3月3日发布国内首款 AI 原生集成开发环境工具2。
          </View>
          <View className="rui-flex-item">
            字节跳动于3月3日发布国内首款 AI 原生集成开发环境工具3。
          </View>
        </View>
        <View className="rui-flex-content">
          <View className="rui-flex-item">
            字节跳动于3月3日发布国内首款 AI 原生集成开发环境工具1。
          </View>
          <View className="rui-flex-item">
            字节跳动于3月3日发布国内首款 AI 原生集成开发环境工具2。
          </View>
          <View className="rui-flex-item">
            字节跳动于3月3日发布国内首款 AI 原生集成开发环境工具3。
          </View>
        </View>
      </View>
    </View>
  );
}
