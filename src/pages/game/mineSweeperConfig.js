// import icon0 from './images/0.bmp';
// import icon1 from './images/1.bmp';
// import icon2 from './images/2.bmp';
// import icon3 from './images/3.bmp';
// import icon4 from './images/4.bmp';
// import icon5 from './images/5.bmp';
// import icon6 from './images/6.bmp';
// import icon7 from './images/7.bmp';
// import icon8 from './images/8.bmp';
// import ask from './images/ask.bmp';
// import blank from './images/blank.bmp';
// import blood from './images/blood.bmp';
// import error from './images/error.bmp';
// import flag from './images/flag.bmp';
// import mine from './images/mine.bmp';
import face_fail from './images/face_fail.png';
import face_normal from './images/face_normal.png';
import face_success from './images/face_success.png';

export default {
  // icon0,
  // icon1,
  // icon2,
  // icon3,
  // icon4,
  // icon5,
  // icon6,
  // icon7,
  // icon8,
  // ask,
  // blank,
  // blood,
  // error,
  // flag,
  // mine,
  face_fail,
  face_normal,
  face_success
}
