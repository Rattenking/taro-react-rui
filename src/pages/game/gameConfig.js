/*
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2023-01-06 10:45:11
 */
const gameList = [
  {
    title: "Taro React 五子连珠消除小游戏",
    url: "../../pages/game/removeFiveBead"
  },
  {
    title: "Taro React 扫雷",
    url: "../../pages/game/mineSweeper"
  },
  {
    title: "Taro React 贪吃蛇",
    url: "../../pages/game/snakeGame"
  },
  {
    title: "Taro React 五子棋",
    url: "../../pages/game/gobang"
  },
  {
    title: "Taro React 俄罗斯方块",
    url: "../../pages/game/tetrisGame"
  },
]
export default gameList;