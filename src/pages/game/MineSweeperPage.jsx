import { View, Text, Image } from '@tarojs/components';
import { useState, useEffect } from 'react';
import './mineSweeper.scss';
import mineSweeperConfig from './mineSweeperConfig';

const MineSweeperPage = (props) => {
  // console.log(mineSweeperConfig)
  return <View className='rui-mine-sweeper-page-content'>
    <Image src={mineSweeperConfig.face_success} className="rui-full" mode="widthFix"></Image>
  </View>
}
export default MineSweeperPage;