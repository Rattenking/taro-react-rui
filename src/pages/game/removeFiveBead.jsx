/*
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2023-01-06 10:47:43
 */
import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View, Text, Canvas } from "@tarojs/components";
import './index.scss';
// import { game } from './removeFiveBead';

class RemoveFiveBead extends Component {
  constructor(props) {
    super(props);
  }
  state = {}
  componentDidMount () {
    console.log(game)
    // game.start();
  }
  // 判断环境返回元素
  getEnvElemHtml () {
    if (process.env.TARO_ENV === "h5") {
      return <View><Canvas className="rui-canvas-content" id="canvas"></Canvas></View>
    }
  }
  render() { 
    return <View className="rui-remove-five-bead-page-content">
      {/* 判断环境返回元素 */}
      {getEnvElemHtml()}
    </View>;
  }
  
}

export default RemoveFiveBead;