import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import './index.scss';
import gameList from './gameConfig';

class GameIndex extends Component {
  constructor(props) {
    super(props);
  }
  state = {  }
  render() { 
    return <View className="rui-game-page-content">
      <View className="rui-list-content">
        {
          gameList.map((item, index) =>
            <View className="rui-flex-ac rui-list-item"
              key={item+index}
              onClick={this.$route.bind(this, { url: item.url })}>
              <Text className="rui-fs30 rui-fg rui-line1">{ item.title }</Text>
              <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
            </View>)
        }
      </View>
    </View>;
  }
}

export default GameIndex;