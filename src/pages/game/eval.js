// const boardSize = 15;

// 初始化棋盘
// let board = Array(boardSize).fill().map(() => Array(boardSize).fill(0));

// 评估函数
function evaluateScore(board, player) {
    let score = 0;
    let boardSize = board.length;
    const directions = [[1, 0], [0, 1], [1, 1], [1, -1]]; // 四个方向：水平、垂直、两个对角线

    // 遍历棋盘上的每一个点
    for (let i = 0; i < boardSize; i++) {
        for (let j = 0; j < boardSize; j++) {
            if (board[i][j] === player) {
                // 检查每个方向
                for (let [dx, dy] of directions) {
                    let count = 1; // 当前点的棋子
                    let blocks = 0; // 阻挡的空位

                    // 向一个方向延伸
                    for (let step = 1; step < 5; step++) {
                        let x = i + step * dx;
                        let y = j + step * dy;
                        if (x >= 0 && x < boardSize && y >= 0 && y < boardSize) {
                            if (board[x][y] === player) {
                                count++;
                            } else if (board[x][y] === 0) {
                                blocks++;
                            } else {
                                break;
                            }
                        }
                    }

                    // 向相反方向延伸
                    for (let step = 1; step < 5; step++) {
                        let x = i - step * dx;
                        let y = j - step * dy;
                        if (x >= 0 && x < boardSize && y >= 0 && y < boardSize) {
                            if (board[x][y] === player) {
                                count++;
                            } else if (board[x][y] === 0) {
                                blocks++;
                            } else {
                                break;
                            }
                        }
                    }

                    // 根据不同的棋型评分
                    if (count === 5) {
                        score += 100000; // 连五
                    } else if (count === 4 && blocks === 0) {
                        score += 10000; // 活四
                    } else if (count === 4 && blocks === 1) {
                        score += 5000; // 眠四
                    } else if (count === 3 && blocks === 0) {
                        score += 1000; // 活三
                    } else if (count === 3 && blocks === 1) {
                        score += 500; // 眠三
                    } else if (count === 2 && blocks === 0) {
                        score += 100; // 活二
                    }
                }
            }
        }
    }

    return score;
}

// const boardSize = 15;
const scores = {
  '连五': 100000,
  '活四': 10000,
  '眠四': 5000,
  '活三': 1000,
  '眠三': 500,
  '活二': 100,
  '眠二': 50,
};

// 初始化棋盘
// let board = Array(boardSize).fill().map(() => Array(boardSize).fill(0));

// 检查是否为有效位置
function isValid(x, y, boardSize) {
  return x >= 0 && x < boardSize && y >= 0 && y < boardSize;
}

// 检查一条线上是否有连续的相同棋子
function checkLine(board, x, y, dx, dy, player) {
  let boardSize = board.length
  let count = 0;
  let empty = 0;
  let opponent = player === 1 ? -1 : 1;

  // 向一个方向检查
  for (let step = 0; step < 5; step++) {
    let newX = x + step * dx;
    let newY = y + step * dy;
    if (isValid(newX, newY, boardSize)) {
      if (board[newX][newY] === player) {
        count++;
      } else if (board[newX][newY] === 0) {
        empty++;
      } else {
        break;
      }
    }
  }

  // 向相反方向检查
  for (let step = 1; step < 5; step++) {
    let newX = x - step * dx;
    let newY = y - step * dy;
    if (isValid(newX, newY, boardSize)) {
      if (board[newX][newY] === player) {
        count++;
      } else if (board[newX][newY] === 0) {
        empty++;
      } else {
        break;
      }
    }
  }

  // 根据连续棋子数量和空位数量评分
  if (count === 5) {
    return scores['连五'];
  } else if (count === 4 && empty === 1) {
    return scores['活四'];
  } else if (count === 4 && empty === 0) {
    return scores['眠四'];
  } else if (count === 3 && empty === 2) {
    return scores['活三'];
  } else if (count === 3 && empty === 1) {
    return scores['眠三'];
  } else if (count === 2 && empty === 3) {
    return scores['活二'];
  } else if (count === 2 && empty === 1) {
    return scores['眠二'];
  }

  return 0;
}

// 计算一个位置的得分
function calculateScoreAtPosition(board, x, y, player) {
  let score = 0;
  const directions = [[1, 0], [0, 1], [1, 1], [1, -1]]; // 四个方向：水平、垂直、两个对角线

  // 检查每个方向
  for (let [dx, dy] of directions) {
    score += checkLine(board, x, y, dx, dy, player);
  }

  return score;
}

// 示例用法
// console.log(calculateScoreAtPosition(7, 7, 1)); // 假设(7, 7)是当前玩家要下的位置，1代表当前玩家


export default calculateScoreAtPosition;
// 示例用法
// console.log(evaluate(board, 1)); // 假设1代表当前玩家的棋子
