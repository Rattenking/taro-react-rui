/*
 * @Description: 模态框
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-10-19 17:10:43
 * @LastEditors: Rattenking
 * @LastEditTime: 2022-10-19 17:28:45
 */
import React, { Component } from "react";
import { View, Text } from '@tarojs/components';
import RuiModal from "../../components/RuiModal/RuiModal";
import './index.scss';


class Overlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowBase: false,
      isShowContent: false,
      isShowBottom: false,
      isShowOpacity: false,
      isShowDuration: false,
    }
  }
  render () {
    let {
      isShowBase,
      isShowContent,
      isShowBottom,
      isShowOpacity,
      isShowDuration
    } = this.state;
    return <View className="rui-modal-page-content">
      {/* 基本案例 */}
      <RuiModal isOpened={isShowBase}
        title="标题"
        content="uView的目标是成为uni-app生态最优秀的UI框架！"
        cancelText="取消"
        confirmText="确定"
        onClose={() => { this.setState({ isShowBase: false }) }} />
      
      <View className="rui-flex-ac rui-list-item"
        onClick={() => { this.setState({isShowBase: true}) }}>
        <Text className="rui-fs30 rui-fg">基本案例</Text>
        <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
      </View>
      <View className="rui-flex-ac rui-list-item"
        onClick={() => { this.setState({isShowContent: true}) }}>
        <Text className="rui-fs30 rui-fg">嵌入内容居中</Text>
        <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
      </View>
      <View className="rui-flex-ac rui-list-item"
        onClick={() => { this.setState({isShowBottom: true}) }}>
        <Text className="rui-fs30 rui-fg">嵌入内容底部</Text>
        <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
      </View>
      <View className="rui-flex-ac rui-list-item"
        onClick={() => { this.setState({isShowOpacity: true}) }}>
        <Text className="rui-fs30 rui-fg">设置透明度</Text>
        <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
      </View>
      <View className="rui-flex-ac rui-list-item"
        onClick={() => { this.setState({isShowDuration: true}) }}>
        <Text className="rui-fs30 rui-fg">设置动画时间</Text>
        <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
      </View>
    </View>
  }
}

export default Overlay;