/*
 * @Description: 模态框
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-10-18 09:55:14
 * @LastEditors: Rattenking
 * @LastEditTime: 2022-10-19 17:11:01
 */
export default {
  navigationBarTitleText: "模态框",
  navigationStyle: "custom",
};