/*
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-11-09 10:38:58
 */
import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import RuiIndexes from "@com/RuiIndexes/RuiIndexes";
import RuiIndexesItem from "@com/RuiIndexes/RuiIndexesItem";
import citys from './citys';
import './index.scss';

class Base extends Component {
  constructor(props) {
    super(props);
  }
  state = {  }
  render() { 
    return <View className="rui-indexes-page-content">
      <View className="rui-indexes-container">
        <RuiIndexes>
          <React.Fragment>
            {
              citys.map((item, index) => 
                <RuiIndexesItem key={item+index} id={item.key}>
                  <View className="rui-indexes-head">{item.title}</View>
                  <React.Fragment>
                    {
                      item.items.map((address, idx) =>
                        <View
                          key={address+idx}
                          className="rui-indexes-li rui-flex-ac rui-list-item rui-fs30">
                          {address.name}
                        </View>)
                    }
                  </React.Fragment>
                </RuiIndexesItem>)
            }
          </React.Fragment>
        </RuiIndexes>
      </View>
    </View>;
  }
}

export default Base;