/*
 * @version: 1.0.0
 * @Author: Rattenking
 * @Date: 2022-11-09 09:30:29
 */
import React, { Component } from "react";
import Taro from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import './index.scss';

class Indexes extends Component {
  constructor(props) {
    super(props);
  }
  state = {  
    list: [
      {
        title: '基本案例',
        url: '../../pages/indexes/base'
      }
    ]
  }
  render () { 
    let { list } = this.state;
    return <View className="rui-indexes-page-content rui-ml30 rui-mr30">
      {
        list.map((item, index) =>
          <View className="rui-flex-ac rui-list-item"
            key={item+index}
            onClick={this.$route.bind(this,{url: item.url})}>
            <Text className="rui-fs30 rui-fg">{ item.title }</Text>
            <Text className="rui-icon rui-icon-arrow-right rui-fa rui-fs30"></Text>
          </View>)
      }
    </View>;
  }
}

export default Indexes;