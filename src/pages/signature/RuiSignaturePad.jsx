import Taro, { useReady, createCanvasContext, nextTick } from '@tarojs/taro'
import { View, Canvas, Button } from '@tarojs/components'
import { useRef, useState } from 'react'
import './index.scss';

const SignaturePad = () => {
  const canvasRef = useRef(null)
  const [ctx, setCtx] = useState()
  const points = useRef([])

  // 初始化 Canvas
  useReady(() => {
    setTimeout(() => {
      const context = createCanvasContext('signatureCanvas', this)
      setCtx(context)
      
      // 设置绘制样式
      context.setStrokeStyle('#000000')
      context.setLineWidth(2)
      context.setLineCap('round')
      context.setLineJoin('round')
    },100)
  })

  // 触摸开始
  const handleStart = (e) => {
    const { x, y } = getEventPosition(e)
    points.current = [{ x, y }]
    ctx?.beginPath()
  }

  // 触摸移动
  const handleMove = (e) => {
    const { x, y } = getEventPosition(e)
    points.current.push({ x, y })
    if (points.current.length > 1) {
      ctx?.moveTo(points.current.at(-1).x, points.current.at(-1).y)
      ctx?.lineTo(x, y)
      ctx?.stroke()
      ctx?.draw(true) // 小程序需要显式调用绘制
    }
  }

  // 获取坐标（兼容多平台）
  const getEventPosition = (e) => {
    console.log('e:',e)
    const touch = e.touches?.[0] || e;
    console.log('touch:',touch)
    return {
      x: touch.x || touch.clientX,
      y: touch.y || touch.clientY
    }
  }

  // 清空画布
  const clear = () => {
    ctx?.clearRect(0, 0, 300, 200)
    ctx?.draw()
    points.current = []
  }

  // 保存签名
  const save = async () => {
    try {
      const res = await Taro.canvasToTempFilePath({
        canvasId: 'signatureCanvas',
        fileType: 'png'
      })
      Taro.saveImageToPhotosAlbum({
        filePath: res.tempFilePath
      })
    } catch (err) {
      console.error('保存失败:', err)
    }
  }

  return (
    <View className='container'>
      <Canvas
        ref={canvasRef}
        canvasId='signatureCanvas'
        id='signatureCanvas'
        disableScroll // 防止滚动干扰
        style={{ width: '300px', height: '200px', border: '1px solid #ddd' }}
        onTouchStart={handleStart}
        onTouchMove={handleMove}
      />
      
      <View className='buttons'>
        <Button onClick={clear}>清空</Button>
        <Button onClick={save}>保存</Button>
      </View>
    </View>
  )
}

export default SignaturePad