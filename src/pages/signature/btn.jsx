import { View, Text, Image } from "@tarojs/components";
import RuiCarousel from "@src/components/RuiCarousel/RuiCarousel";
import "./index.scss";

const Btn = (props) => {
  return (
    <View className="rui-limit-page-content">
      <View className="rui-fwb rui-fs36">默认</View>
      <View className="rui-flex-fw">
        <View className="rui-btn">按钮</View>
      </View>
      <View className="rui-fwb rui-fs36">无边框</View>
      <View className="rui-flex-fw">
        <View className="rui-btn rui-btn-none rui-bg1">按钮</View>
      </View>
      <View className="rui-fwb rui-fs36">圆角</View>
      <View className="rui-flex-fw">
        <View className="rui-btn rui-btn-none rui-circle-btn rui-bg2">
          按钮
        </View>
      </View>
      <View className="rui-fwb rui-fs36">渐变</View>
      <View className="rui-flex-fw">
        <View className="rui-btn rui-btn-none rui-bg3 rui-mb20">按钮</View>
      </View>
      <RuiCarousel>
        <View style="white-space: nowrap;">
          我将检查代码中的JSX标签闭合位置，确保闭合括号与开始标签对齐。首先会分析当前文件的代码风格，然后统一调整所有JSX标签的闭合位置，保持代码风格一致。最后会检查所有相关文件，确保整个项目的JSX标签闭合位置规范统一。
        </View>
      </RuiCarousel>
    </View>
  );
};
export default Btn;
