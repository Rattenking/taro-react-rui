import { View } from "@tarojs/components";
import "./index.scss";

const Loading = (props) => {
  return (
    <View className="rui-limit-page-content">
      <View className="rui-grid-content">
        <View className="rui-grid-item">
          <View className="rui-loading-dot"></View>
        </View>
        <View className="rui-grid-item">
          <View className="rui-nb-spinner"></View>
        </View>
        <View className="rui-grid-item">
          <View className="rui-top-spinner"></View>
        </View>
        <View className="rui-grid-item">
          <View className="rui-tb-spinner"></View>
        </View>
        <View className="rui-grid-item">
          <View className="rui-tl-spinner2"></View>
          <View className="rui-tl-spinner"></View>
        </View>
        <View className="rui-grid-item">
          <View className="rui-reverse-spinner"></View>
        </View>
        <View className="rui-grid-item">
          <View className="linearBorder">动态边框</View>
        </View>
      </View>
    </View>
  );
};
export default Loading;
